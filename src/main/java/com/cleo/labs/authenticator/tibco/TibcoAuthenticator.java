package com.cleo.labs.authenticator.tibco;

import com.cleo.security.hashing.Authenticator;
import com.google.common.hash.Hashing;
import com.google.common.io.BaseEncoding;

public class TibcoAuthenticator implements Authenticator {
    public static final String SCHEME = "tibco";

    public String getSchemeName() {
        return SCHEME;
    }

    public boolean verify(String username, String password, String hashScheme) {
        String[] parsed = hashScheme.split(":"); // scheme:iterations:salt:hash
        byte[] salt = BaseEncoding.base16().decode(parsed[2].toUpperCase());
        String candidate = Hashing.sha256()
                                  .newHasher()
                                  .putBytes(salt)
                                  .putBytes(password.getBytes())
                                  .hash()
                                  .toString();
        return parsed[3].equals(candidate);
    }
}
