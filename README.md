# Tibco Authenticator #

This is a custom password matching class for Cleo Harmony intended for use
passwords migrated from Tibco MFT (fka Proginet) Internet Server.

# Installation #

1. Clone the project and build `tibco-authenticator-5.4.0.0.jar` using `mvn package`.
2. Copy `tibco-authenticator-5.4.0.0.jar` to `lib/api`.
3. Edit `conf/system.properties` and add `cleo.password.validator.tibco=com.cleo.labs.authenticator.tibco.TibcoAuthenticator`
4. Stop and restart your VersaLex server

# Use #

The Tibco Internet Server uses a single round SHA256 hashing scheme with a 64-bit salt.  Each
password entry in the database consists of a 16 character hexadecimal representation of the salt
followed by a space and the 64 character hexadecimal representation of the SHA 256 hash.

Passwords in Cleo Harmony are recorded as follows:
```
scheme:iterations:salt:hash
```
In this case, the `scheme` is `tibco` and `iterations` is always `1`.  When migrating users,
copy the 16 character salt and 64 character hash into the the Cleo password field.  For example
if the Tibco database contains:
```
91ea5226b1f18822 bda72b68d0c1a091494cf14f01b44af1096d96e35eb92521938057a2597f64ee
```
format the Cleo password as follows:
```
tibco:1:91ea5226b1f18822:bda72b68d0c1a091494cf14f01b44af1096d96e35eb92521938057a2597f64ee
```

Migrated users must have their passwords set through the Cleo Java API or by direct
manipulation of the XML user group file (but not through the UI).  When users change
their passwords, they will be updated to the built-in `vlpdkdf2` scheme.
